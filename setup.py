from setuptools import find_packages, setup

with open(".version.txt") as version_file:
    version = version_file.read().strip()

setup(
    name="docker-version-filter",
    version=version,
    author="Bengt Giger",
    author_email="bgiger@ethz.ch",
    description="Determine latest stable version of a docker registry image",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    url="https://gitlab.ethz.ch/bgiger/docker-version-filter",
    install_requires=["click"],
)
