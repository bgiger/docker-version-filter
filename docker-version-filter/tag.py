from datetime import datetime
from connection import RegistryConnection


class DockerTag(object):
    name = ""
    tag_status = ""
    tag_last_pushed = ""

    def __init__(self, name, tag_status, tag_last_pushed):
        self.name = name
        self.tag_status = tag_status
        self.tag_last_pushed = datetime.fromisoformat(tag_last_pushed[:-1])


class DockerTagList(object):
    tags = {}

    def __init__(self, url):
        conn = RegistryConnection(url)
        for tag in conn.get_tags():
            self.tags[tag["name"]] = DockerTag(
                tag["name"], tag["tag_status"], tag["tag_last_pushed"]
            )
