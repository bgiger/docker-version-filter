from filter import TagFilter


class JupyterFilter(TagFilter):
    def stable_tag(self, tags):
        del_keys = []
        for tag_id in tags.tags.keys():
            # delete development tags
            if tags.tags[tag_id].name.find("dev") >= 0:
                del_keys.append(tag_id)

        del tags.tags["master"]
        del tags.tags["latest"]
        for key in del_keys:
            del tags.tags[key]

        latest_tag = None
        for tag in tags.tags.keys():
            if latest_tag is None:
                latest_tag = tags.tags[tag]
                continue
            if tags.tags[tag].tag_last_pushed > latest_tag.tag_last_pushed:
                latest_tag = tags.tags[tag]

        return latest_tag
