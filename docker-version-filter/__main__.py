import click
import logging
from tag import DockerTagList
from JupyterFilter import JupyterFilter


@click.command()
@click.argument("url")
@click.option(
    "--debug/--no-debug", default=False, type=bool, help="Enable debugging output"
)
def run(url, debug):

    if debug:
        logging.getLogger().setLevel(logging.DEBUG)

    tag_list = DockerTagList(url)
    sieve = JupyterFilter()
    logging.debug(sieve.stable_tag(tag_list).name)


run()
