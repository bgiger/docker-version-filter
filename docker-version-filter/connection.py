import requests
import json


class RegistryConnection(requests.Session):
    url = ""

    def __init__(self, registry):
        requests.Session.__init__(self)
        self.url = registry

    def get_page(self, url):
        res = requests.Session.get(self, url)
        if res.status_code >= 200 or res.status_code < 400:
            return res.content
        else:
            raise requests.ConnectionError

    def get_tags(self):
        tags = []
        page = json.loads(self.get_page(self.url))
        tag_list = page["results"]

        while True:
            for tag in tag_list:
                tags.append(tag)

            if page["next"] is None:
                break
            else:
                page = json.loads(self.get_page(page["next"]))
        return tags
